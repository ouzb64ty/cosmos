var controllerList = function() {
	var newCameraSubmit = document.querySelector('#newCameraSubmit');
	var deleteCamera = document.querySelectorAll('.deleteCamera');
	var xhttp = new XMLHttpRequest();

	newCameraSubmit.addEventListener('click', function() {
		var request = 'http://127.0.0.1:8080/api/cameras/' + encodeURIComponent(document.getElementById('newCamera').value);

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			    location.reload();
			}
	    };
		xhttp.open("POST", request, true);
	    xhttp.send();
	});

	for (var i = 0; i < deleteCamera.length; i++) {

		deleteCamera[i].addEventListener('click', function(obj) {
			var request = 'http://127.0.0.1:8080/api' + obj.target.id;

			xhttp.onreadystatechange = function() {
				location.reload();
		    };
			xhttp.open("DELETE", request, true);
		    xhttp.send();
		});

	}
};

window.controllerList = controllerList;