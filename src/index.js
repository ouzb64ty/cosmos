/* Database --------------------------------------------------------------- */

const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/cosmos';

/* Reset redis db */
const redis = require('redis');
const client = redis.createClient();
client.flushdb();

MongoClient.connect(url, function(err, database) {
    if (err) throw err;

    const db = database.db('cosmos');
    const express = require('express');
    const app = express();
    const server = app.listen(8080);
    const resources = require('./resources');
    const path = require('path');

/* Sockets ---------------------------------------------------------------- */

    const allowedOrigins = "http://localhost:* http://127.0.0.1:*";
    const io = require('socket.io');
    const sequencerIo = io(server, { origins: allowedOrigins })

/* Configurations --------------------------------------------------------- */

    app.use(express.static('src/public'));
    app.use(express.static('bower_components'));
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'pug');

/* API -------------------------------------------------------------------- */
    
    app.get('/api/cameras', (req, res) => {
		resources.Cameras.getCameras(db, req, res, undefined, true);
    });
    app.get('/api/cameras/geojson', (req, res) => {
		resources.Cameras.getCamerasGeoJSON(db, req, res, undefined, true);
    });
    app.get('/api/cameras/geojson/:id', (req, res) => {
		resources.Cameras.getCameraGeoJSON(db, req, res, undefined, true);
    });
    app.get('/api/cameras/:id', (req, res) => {
		resources.Cameras.getCamera(db, req, res, undefined, true);
    });
    app.post('/api/cameras/:url', (req, res) => {
		resources.Cameras.addCamera(db, req, res, undefined, true);
    });
    app.delete('/api/cameras', (req, res) => {
		resources.Cameras.deleteCameras(db, req, res, undefined, true);
    });
    app.delete('/api/cameras/:id', (req, res) => {
		resources.Cameras.deleteCamera(db, req, res, undefined, true);
    });
    app.put('/api/cameras/detection/:id/:value', (req, res) => {
        const { spawn } = require('child_process');

		resources.Cameras.putCameraDetection(db, req, res, sequencerIo, true);
    });

    app.get('/api/sequences/:id', (req, res) => {
        resources.Cameras.getSeqByCamId(db, req, res, undefined, true);
    });
    app.get('/api/sequences', (req, res) => {
        resources.Cameras.getSequences(db, req, res, undefined, true);
    });
    app.delete('/api/sequences', (req, res) => {
        resources.Cameras.deleteAllSeq(db, req, res, undefined, true);
    });
    
/* Router ----------------------------------------------------------------- */
    
    app.get('/', (req, res) => {
        resources.Cameras.getSequences(db, req, res, (sequences) => {
            res.render('index.pug', {
                items: {
                    loader: 'controllerIndex();',
                    sequences: sequences
                }
            }, false);
        });
    });
    app.get('/analyze', (req, res) => {
		res.render('analyze.pug', {
	    	items: {
                loader: 'controllerAnalyze();'
	    	}
		});
    });
    app.get('/list', (req, res) => {
		const camera = resources.Cameras.getCameras(db, req, res, (cameras) => {
	    	res.render('list.pug', {
	    		items: {
		    		cameras: cameras,
                    loader: 'controllerList();'
	    		}
	    	});
		});
    });
    app.get('/camera/:id', (req, res) => {
    	const camera = resources.Cameras.getCamera(db, req, res, (camera) => {
            client.exists(req.params.id, (err, reply) => {
                /* Call sequences resource and append in rendering params */
                resources.Cameras.getSeqByCamId(db, req, res, (sequences) => {
                    res.render('camera.pug', {
                        items: {
                            loader: "controllerCamera('" + req.params.id + "');",
                            detection: camera.detection,
                            url: camera.geojson.properties.url,
                            title: camera.geojson.properties.country + ' - ' + camera.geojson.properties.zip + ' - ' + camera.geojson.properties.city,
                            sequences: sequences,
                            detectionActive: reply
                        }
                    }, false);

                });
            });
    	});
    });
});