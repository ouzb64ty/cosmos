Cosmos
======

(See new versions from orizon, vision, orizon-supervision, vision-supervision for IP Camera and videos analyses, contact me for more informations)

The Boat Detector Application.
Detection algorithm (pjreddie/darknet fork of AlexeyAB) works on open world cameras.

# Prérequis

- Ubuntu v16.04
- Curl :
	- `sudo apt-get install curl`
- Node.js v8.9.4 :
	- `curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
	sudo apt-get install -y nodejs`
- Yarn (gestionnaire de dépendances pour nodejs) :
	- Suivre les étapes d'installation sur le site officiel du projet (yarnpkg.org)
- Git (Outil de versionning) :
	- Installer depuis l'apt : `sudo apt-get install git`
- Mongodb (Base de données) :
	- Idem : `sudo apt-get install mongodb`
- OpenCV (pour Darknet) :
	- `sudo apt-get install libopencv-dev libcv-dev`
- FFMPEG (pour Screenshot les caméras) :
	- `sudo apt-get install ffmpeg`

# Installation

- Install from git depot :
	- `git clone https://framagit.org/ouzb64ty/cosmos.git`
- Activer le service Mongodb :
	- `sudo systemctl start mongodb`
- Installer toutes les dépendances :
	- `yarn install`
- Générer bundle.js (Webpack) :
	- `yarn build`
- Darknet (open source detection system) :
	- `git clone https://github.com/AlexeyAB/darknet.git`
	- Modify `OPENCV` and `GPU` variable in Makefile to 1.
	- Type `make` to compile.
- Activer le serveur :
	- `yarn start`
	- ou `node src/index.js`

_Just typing `ls node_modules` for list Nodejs dependancies._

Now, go at 127.0.0.1:8080 url..

# Cameras

For adding cameras in Mongo database, you should just execute `tools/scripts/faddcam`
with one parameter (cameras list file).
Current cameras list can be display with `ls tools/cameras`.

## Example

`node tools/scripts/faddcam.js tools/cameras/marine.txt`

# Contacts

ouzb64ty@protonmail.com
framel@student.42.fr
jcazako@student.42.fr